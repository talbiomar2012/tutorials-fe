import axios from "axios";

export default axios.create({
  baseURL: "http://CHANGE-ME:PORT/tutorials-be/api",
  headers: {
    "Content-type": "application/json"
  }
});